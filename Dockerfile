FROM mambaorg/micromamba

## ensure locale is set during build
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

USER root

COPY --chown=$MAMBA_USER:$MAMBA_USER env.yaml /tmp/env.yaml
RUN micromamba install -y -n base -f /tmp/env.yaml && \
    micromamba clean --all --yes
ARG MAMBA_DOCKERFILE_ACTIVATE=1

USER $MAMBA_USER
# The following line is mandatory:
CMD ["sh", "-c", \
     "jupyter lab --notebook-dir=/home/$MAMBA_USER --ip=0.0.0.0 --no-browser \
      --allow-root --port=8888 --LabApp.token='' --LabApp.password='' \
      --LabApp.allow_origin='*' --LabApp.base_url=${NB_PREFIX}"]
