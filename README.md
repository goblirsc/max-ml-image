# Max Ml

Machine learning docker image for Max. Maximum fun!

## Getting started

```bash
docker run --rm -it -p 8888:8888 gitlab-registry.cern.ch/pgadow/max_ml:latest
```

and inside the container start notebook with

```bash
jupyter notebook --ip 0.0.0.0 --no-browser --allow-root
```

Then open link in webbrowser.
